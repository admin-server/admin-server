'use client';

import '@/shared/styles/main.scss';
// import type { Metadata } from 'next';
import { Inter } from 'next/font/google';

import { store } from '@/store/store';
import { Provider } from 'react-redux';

const inter = Inter({ subsets: ['latin'] });

// export const metadata: Metadata = {
//   title: 'Create Next App',
//   description: 'Generated by create next app',
// };

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <Provider store={store}>
      <html lang="en">
        <body className={inter.className} cz-shortcut-listen="true">{children}</body>
      </html>
    </Provider>
  );
}

